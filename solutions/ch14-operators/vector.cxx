#include <iostream>
#include <string>

using std::cout;

template<typename T=int> requires std::is_arithmetic_v<T>
struct Vector {
    T x{}, y{}, z{};
    Vector() = default;
    Vector(T x, T y, T z) : x{x}, y{y}, z{z} {}
    auto operator<=>(Vector const&) const = default;
};

template<typename T=int>
auto operator<<(std::ostream& os, Vector<T> const& v) -> std::ostream& {
    return os << "[" << v.x << ", " << v.y << ", " << v.z << "]";
}

template<typename T=int, typename U>
requires std::is_arithmetic_v<U>
auto operator*(Vector<T> const& v, U c) -> Vector<T> {
    return {c * v.x, c * v.y, c * v.z};
}

template<typename T=int, typename U>
requires std::is_arithmetic_v<U>
auto operator*(U c, Vector<T> const& v) {
    return v * c;
}

template<typename T=int>
auto operator*(Vector<T> const& a, Vector<T> const& b) {
    return a.x * b.x + a.y * b.y + a.z * b.z;
}

template<typename T=int>
auto operator+(Vector<T> const& a, Vector<T> const& b) -> Vector<T> {
    return {a.x + b.x, a.y + b.y, a.z + b.z};
}

template<typename T=int>
auto operator-(Vector<T> const& a, Vector<T> const& b) -> Vector<T> {
    return {a.x - b.x, a.y - b.y, a.z - b.z};
}

int main() {
    cout << std::boolalpha;
    auto v1 = Vector<>{10, 20, 30};
    cout << "v1: " << v1 << "\n";

    auto v2 = Vector<>{5, 15, 25};
    cout << "v2: " << v2 << "\n";

    cout << "v1 == v2: " << (v1 == v2) << "\n";
    cout << "v1 != v2: " << (v1 != v2) << "\n";
    cout << "v1 < v2: " << (v1 < v2) << "\n";
    cout << "v1 > v2: " << (v1 > v2) << "\n";

    cout << "v1 * 42: " << (v1 * 42) << "\n";
    cout << "42 * v1: " << (42 * v1) << "\n";

    cout << "v1 * v2: " << (v1 * v2) << "\n";
    cout << "v1 + v2: " << (v1 + v2) << "\n";
    cout << "v1 - v2: " << (v1 - v2) << "\n";

    {
        //auto v = Vector<std::string>{};
        //template constraint failure for ‘template<class T>  requires  is_arithmetic_v<T> struct Vector’
    }
}
