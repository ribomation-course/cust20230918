#include <iostream>
#include <string>
#include <memory>

using namespace std::string_literals;
using std::string;
using std::cout;

class Person {
    string name{};
    unsigned age{};
public:
    Person() {
        cout << "Person{} @ "s << this << '\n';
    }

    Person(string n, unsigned a) : name{n}, age{a} {
        cout << "Person{"s << name << ", "s << age << "} @ "s << this << '\n';
    }

    ~Person() {
        cout << "~Person{"s << name << "} @ "s << this << '\n';
    }

    auto getAge() const { return age; }

    auto getName() const { return name; }

    void setAge(unsigned a) { age = a; }

    void setName(string n) { name = n; }

    auto toString() const {
        return "Person{name='"s + name + "', age="s + std::to_string(age) + "}"s;
    }
};

Person anna{"Anna Conda"s, 37};

int main() {
    cout << "[main] enter\n";
    {
        Person p1;
        cout << "p1: " << p1.toString() << '\n';

        Person p2{"Nisse Hult"s, 42};
        cout << "p2: " << p2.toString() << '\n';

        Person* p3 = new Person{"Per Silja"s, 77};
        cout << "p3: " << p3->toString() << '\n';
        delete p3;

        auto p4 = std::unique_ptr<Person>{new Person{"Pippi Långstrump"s, 9}};
        cout << "p4: " << p4->toString() << '\n';

        auto p5 = std::make_unique<Person>("Emil"s, 23);
        cout << "p5: " << p5->toString() << '\n';
    }
    cout << "[main] exit\n";
}
