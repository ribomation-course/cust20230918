#include <iostream>
#include <cassert>
#include "text-utils.hxx"

using namespace std::string_literals;
using std::cout;
namespace t = ribomation::text;

int main() {
    assert(t::lowercase("aBcDEF"s) == "abcdef"s);
    assert(t::uppercase("aBcDEF"s) == "ABCDEF"s);
    assert(t::capitalize("aBcDEF"s) == "Abcdef"s);
    assert(t::strip("ABC 123 !.hej"s) == "ABChej"s);
    assert(t::truncate("abcdef"s, 3) == "abc"s);
    assert(t::truncate("abc"s, 3) == "abc"s);
    assert(t::truncate("a"s, 3) == "a"s);
    cout << "All tests passed OK\n";
}
