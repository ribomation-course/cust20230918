#pragma once

#include <string>
#include <cctype>

namespace ribomation::text {
    using std::string;
    using namespace std::string_literals;

    auto lowercase(string const& s) -> string {
        auto result = ""s;
        for (auto ch: s) result += static_cast<char>(::tolower(ch));
        return result;
    }

    auto uppercase(string const& s) -> string {
        auto result = ""s;
        for (auto ch: s) result += static_cast<char>(::toupper(ch));
        return result;
    }

    auto capitalize(string const& s) -> string {
        return uppercase(s.substr(0, 1)) + lowercase(s.substr(1));
    }

    auto strip(string const& s) -> string {
        auto result = ""s;
        for (auto ch: s) if (::isalpha(ch)) result += ch;
        return result;
    }

    auto truncate(string const& s, unsigned width) -> string {
        if (s.size() <= width) return s;
        return s.substr(0, width);
    }
}
