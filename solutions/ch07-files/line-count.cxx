#include <iostream>
#include <algorithm>
#include "counter.hxx"
#include "each-line.hxx"

using namespace std::string_literals;
namespace io = ribomation::io;

int main(int argc, char** argv) {
    using std::cout;

    if (argc == 1) {
        auto counter = io::Counter{"stdin"s};
        io::each_line(std::cin, [&counter](auto const& line) {
            counter.update(line);
        });
        counter.print(cout);
        return 0;
    }

    auto total = io::Counter{"total"s};
    std::for_each(argv + 1, argv + argc, [&total](auto filename) {
        auto counter = io::Counter{filename};
        io::each_line(filename, [&counter](auto const& line) {
            counter.update(line);
        });
        total.update(counter);
        counter.print(cout);
    });
    total.print(cout);
}
