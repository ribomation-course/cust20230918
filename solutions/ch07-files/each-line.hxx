#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <functional>

namespace ribomation::io {
    using namespace std::string_literals;
    using std::istream;
    using std::string;
    using std::function;

    inline void each_line(istream& in, function<void(string const&)> const& handleLine) {
        for (string line; std::getline(in, line);) {
            handleLine(line);
        }
    }

    inline void each_line(string const& filename, function<void(string const&)> const& handleLine) {
        auto file = std::ifstream{filename};
        if (!file) throw std::runtime_error{"Cannot open file: "s + filename};
        each_line(file, handleLine);
    }
}
