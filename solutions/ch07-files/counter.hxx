#pragma once
#include <iostream>
#include <string>
#include <utility>

namespace ribomation::io {
    using std::string;
    using std::ostream;

    struct Counter {
        string name;
        unsigned lines = 0;
        unsigned words = 0;
        unsigned chars = 0;

        explicit Counter(string  name) : name{std::move(name)} {}

        void update(string const& line) {
            ++lines;
            chars += line.size();
            for (auto i = 0U; i < line.size(); ++i) {
                if (std::isspace(line[i])) {
                    ++words;
                    while (i < line.size() && std::isspace(line[i])) ++i;
                }
            }
        }

        void update(Counter const& rhs) {
            lines += rhs.lines;
            words += rhs.words;
            chars += rhs.chars;
        }

        void print(ostream& out) const {
            out << lines << '\t' << words << '\t' << chars << '\t' << name << '\n';
        }

    };

}
