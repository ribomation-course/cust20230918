#include <iostream>
#include "each-line.hxx"

namespace io = ribomation::io;
using namespace std::string_literals;

int main() {
    auto lineno = 1U;
    io::each_line("./each-line-test.cxx"s, [&lineno](auto line) {
        std::cout << lineno++ << ": " << line << "\n";
    });
}
