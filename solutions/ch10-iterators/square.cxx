#include <iostream>
#include <iterator>

int main() {
    using std::cout;
    auto in = std::istream_iterator<double>{std::cin};
    auto eof = std::istream_iterator<double>{};
    auto out = std::ostream_iterator<long>{cout, " "};

    for (; in != eof; ++in, ++out) {
        auto n = *in;
        *out = static_cast<long>(n * n);
    }
    cout << "\n";
}
