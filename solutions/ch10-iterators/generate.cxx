#include <iostream>
#include <string>
#include <random>
#include <algorithm>
#include <iterator>

int main(int argc, char** argv) {
    using std::cout;
    using std::stoi;
    using std::ostream_iterator;

    auto n = (argc == 1) ? 10U : stoi(argv[1]);
    auto r = std::random_device{};
    auto gauss = std::normal_distribution<double>{10, 5};
    std::generate_n(ostream_iterator<double>{cout, "\n"}, n, [&] { return gauss(r); });
}
