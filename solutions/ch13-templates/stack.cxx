#include <iostream>
#include <string>
using std::cout;
using std::string;
using namespace std::string_literals;

template<typename ElemType=int, unsigned CAPACITY=10U>
class Stack {
    ElemType stk[CAPACITY]{};
    unsigned top = 0;
public:
    auto capacity() const { return CAPACITY;}
    auto size() const { return top; }

    bool empty() const { return size() == 0; }
    bool full() const { return size() == capacity();}

    void push(ElemType x) { stk[top++] = x; }
    ElemType pop() { return stk[--top]; }
};

template<typename ElemType=int, unsigned CAPACITY=10U>
void drain(Stack<ElemType, CAPACITY>& stk) {
    while (!stk.empty()) cout << stk.pop() << " ";
    cout << "\n";
}

int main() {
    {
        cout << "-- Stack<int, 10> --\n";
        auto stk = Stack<>{};
        for (auto k = 1; !stk.full(); ++k) stk.push(k);
        drain(stk);
    }

    {
        cout << "-- Stack<string, 10> --\n";
        auto stk = Stack<string>{};
        for (auto k = 1; !stk.full(); ++k) stk.push("nisse-"s + std::to_string(k));
        drain(stk);
    }
}
