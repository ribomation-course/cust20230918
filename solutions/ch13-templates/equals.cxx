#include <iostream>
#include <string>
#include <cstring> //string.h --> strcmp, strcpy, strlen, ...
#include <type_traits>
#include <cassert>

using namespace std::string_literals;

template<typename Type>
bool equals(Type a, Type b, Type c) {
    return a == b && b == c;
}

template<typename Type>
bool equals(Type a, Type b) {
    if constexpr (std::is_integral_v<Type>) {
        return a == b;
    } else if constexpr (std::is_same_v<Type, std::string>) {
        return a == b;
    } else if constexpr (std::is_convertible_v<Type, char const*>) {
        char const* A = a;
        char const* B = b;
        return ::strcmp(A, B) == 0;
    } else if constexpr (std::is_floating_point_v<Type>) {
        constexpr Type eps = 0.000001;
        return ::abs(a - b) < eps;
    } else {
        return false;
    }
}


int main() {
    assert(equals(42, 2 * 21, 2 * 3 * 7));
    assert(equals("wi-fi"s, "wi"s + "-"s + "fi"s, "wi-fi stereo"s.substr(0, 5)));

//    assert(equals(42, 42U, 42L));
    assert(equals(42.0, 2.0 * 21.0, 84.0 / 2.0));

    std::cout << "all tests passed \n";
}
