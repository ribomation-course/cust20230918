cmake_minimum_required(VERSION 3.25)
project(ch05_refs)

set(CMAKE_CXX_STANDARD 17)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(refs app.cxx)
target_compile_options(refs PRIVATE ${WARN})


