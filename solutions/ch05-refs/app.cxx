#include <iostream>
void fn(int a, int& b, int&& c) {
    a *= 10;
    b *= 20;
    c *= 30;
}

int main() {
    auto number = 42;
    std::cout << "number = " << number << "\n";
    fn(number, number, number*5);
    std::cout << "number = " << number << "\n";
}
