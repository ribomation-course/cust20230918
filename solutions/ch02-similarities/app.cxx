#include <iostream>

int main() {
    using std::cout;
    using std::cin;

    auto count = 0U;
    auto sum = 0.0;
    auto min = 999999U;
    auto max = 0U;
    do {
        cout << "Number? ";
        auto number = 0U;
        cin >> number;
        if (number == 0) break;

        ++count;
        sum += number; // sum = sum + number;
        if (number < min) min = number;
        if (number > max) max = number;
    } while (true);

    // while (true) ...
    // for (;;) ...

    cout << "count: " << count << "\n";
    cout << "average: " << sum / count << "\n";
    cout << "min/max: " << min << "/" << max << "\n";
}
