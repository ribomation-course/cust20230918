cmake_minimum_required(VERSION 3.25)
project(ch02_similarities)

set(CMAKE_CXX_STANDARD 17)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(similarities app.cxx)
target_compile_options(similarities PRIVATE ${WARN})


