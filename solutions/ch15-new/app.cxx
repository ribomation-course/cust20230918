#include <iostream>
#include <string>
#include <vector>
#include <utility>

using std::cout;
using std::string;
using namespace std::string_literals;

class Person {
    string name{};
    unsigned age{};
    inline static int count = 0;
public:
    Person() { ++count; }
    ~Person() { --count; }
    Person(string name, unsigned int age) : name(std::move(name)), age(age) { ++count; }
    Person(Person const& rhs) : name{rhs.name}, age{rhs.age} { ++count; }

    static int getCount() { return count; }

    friend std::ostream& operator<<(std::ostream& os, const Person& person) {
        os << "Person{name: " << person.name << ", age: " << person.age << "}";
        return os;
    }
};

//int Person::count=0;

int main() {
    {
        auto pers = std::vector<Person*>{};
        auto const N = 5U;
        for (auto k = 0U; k < N; ++k)
            pers.push_back(new Person{"Nisse"s + std::to_string(k + 1), (k + 20) % 80});
        cout << "# persons: " << Person::getCount() << "\n";

        for (auto ptr: pers) cout << *ptr << "\n";

        for (auto ptr: pers) delete ptr;
    }
    cout << "# persons: " << Person::getCount() << "\n";

    {
        auto pers = std::vector<Person>{5U};
        cout << "# persons: " << Person::getCount() << "\n";
        for (auto ptr: pers) cout << ptr << "\n";
        for (auto& ptr: pers) cout << ptr << "\n";
        for (auto const& ptr: pers) cout << ptr << "\n";
        for (auto&& ptr: pers) cout << ptr << "\n";
    }
    cout << "# persons: " << Person::getCount() << "\n";
}
