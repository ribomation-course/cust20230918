
#include <iostream>
#include <string>
#include "numbers.hxx"

using std::cout;
using std::endl;
using std::stoi;
using namespace ribomation;

int main(int argc, char** argv) {
    auto n = (argc==1) ? 10 : std::stoi(argv[1]);

    auto s = /*ribomation::*/sum(n);
    cout << "sum(" << n << ") = " << s << endl;

    //using ribomation::prod;
    auto p = prod(n);
    cout << "prod(" << n << ") = " << p << endl;
}
