cmake_minimum_required(VERSION 3.25)
project(ch04_differences)

set(CMAKE_CXX_STANDARD 17)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(differences
        numbers.hxx numbers.cxx
        app.cxx)
target_compile_options(differences PRIVATE ${WARN})


