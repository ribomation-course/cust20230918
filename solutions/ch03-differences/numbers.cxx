
namespace ribomation {
    int global = 42;
    int zero = 0;
    long uninitialized;

    long sum(int n) {
        long s = 0;
        for (int i = 1; i <= n; ++i) s += i;
        return s;
    }

    long prod(int n) {
        long p = 1;
        for (int i = 1; i <= n; ++i) p *= i;
        return p;
    }
}
