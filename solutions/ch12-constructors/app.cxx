#include <iostream>
#include <cstring>

using std::cout;

inline char* copy(char const* s) {
    if (s == nullptr) return nullptr;
    return ::strcpy(new char[::strlen(s) + 1], s);
}


class Person {
    char* name;
    unsigned age;
public:
    Person() : name{nullptr}, age{0} {
        cout << "+Person{} @ " << this << "\n";
    }

    Person(char const* n, unsigned a) : name{copy(n)}, age{a} {
        cout << "+Person{" << name << ", " << age << "} @ " << this << "\n";
    }

    ~Person() {
        cout << "~Person{" << (name ? name : "???") << "} @ " << this << "\n";
        if (name) delete[] name;
    }

    Person(Person const& rhs) : name{copy(rhs.name)}, age{rhs.age} {
        cout << "+Person{Person const&} @ " << this << "\n";
    }

    Person(Person&& rhs) noexcept: name{rhs.name}, age{rhs.age} {
        rhs.name = nullptr;
        rhs.age = 0;
        cout << "+Person{Person&&} @ " << this << "\n";
    }

    void print(char const* prefix, std::ostream& out) const {
        out << prefix << ": Person{" << (name ? name : "???") << ", " << age << "} @ " << this << "\n";
    }

    auto incrAge() { return ++age; }
};

void fnByRef(Person& param) {
    cout << "[fnByRef] enter\n";
    param.incrAge();
    param.print("[fnByRef]", cout);
    cout << "[fnByRef] exit\n";
}

void fnByValue(Person param) {
    cout << "[fnByValue] enter\n";
    param.incrAge();
    param.print("[fnByValue]", cout);
    cout << "[fnByValue] exit\n";
}

Person fnRetByValue() {
    cout << "[fnRetByValue] enter\n";
    Person p{"Kalle Anka", 42};
    p.print("[fnRetByValue]", cout);
    fnByValue(p);
    fnByRef(p);
    cout << "[fnRetByValue] exit\n";
    return p;
}

int main() {
    auto nisse = Person{"Nisse", 42};
    nisse.print("[main]", cout);

    fnByValue(nisse);
    fnByRef(nisse);

    fnByValue(std::move(nisse));
    nisse.print("[main]", cout);

    auto kalle = fnRetByValue();
    kalle.print("[main]", cout);
}
