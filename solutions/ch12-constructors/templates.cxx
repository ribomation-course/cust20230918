
template<typename T>
T abs(T x) { return x < 0 ? -x : x; }

template<typename T>
struct Dummy {
    T value;
    Dummy(T v) : value(v) {}
    void update(T v) { value += v; }
};

int main() {
    auto r1 = abs<int>(-42);
    auto r2 = abs<double>(-42);

    auto d1 = Dummy<int>(42);
    d1.update(17);

    auto d2 = Dummy<double>(42);
    d2.update(3.1415);
}
