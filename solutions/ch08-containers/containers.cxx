#include <iostream>
#include <vector>
#include <string>

using std::vector;
using std::string;
using std::cout;

template<typename T>
auto operator<<(std::ostream& os, vector<T> const& v) -> std::ostream& {
    os << '[';
    auto first = true;
    for (auto const& e: v) {
        if (first) first = false; else os << ',';
        os << e;
    }
    return os << ']';
}

struct Person {
    string name;
    unsigned  age;

    Person(const string& name, unsigned age) : name(name), age(age) {
    }
};

auto operator<<(std::ostream& os, Person const& p) -> std::ostream& {
    return os << p.name << '(' << p.age << ')';
}

int main() {
    vector<int> v1;
    cout << v1 << '\n';

    auto v2 = vector<int>{};
    cout << v2 << '\n';

    auto v3 = vector<int>(10);
    cout << v3 << '\n';

    auto v4 = vector<int>(10, 42);
    cout << v4 << '\n';

    int nums[] = {1, 2, 3, 4, 5};
    auto const N = sizeof(nums) / sizeof(nums[0]);
    auto v5 = vector<int>(nums, nums + N);
    cout << v5 << '\n';

    auto v6 = vector<double>(v5.begin(), v5.end());
    cout << v6 << '\n';

    auto v7 = vector<string>{"C++", "is", "cool"};
    cout << v7 << '\n';

    auto v8 = vector<Person>{};
    // 1
    auto nisse = Person{"Nisse Hult", 42};
    v8.push_back(nisse);

    // 2
    v8.push_back(Person{"Kalle Anka", 43});

    // 3
    v8.emplace_back("Kajsa Anka", 44);
    v8.emplace_back(nisse);

    auto r = v8.back();
    v8.pop_back();

    v4.insert(v4.begin() , 99);

    v4.reserve(1000000);
//    for (...k ... 1000000 ...) {
//        v4.push_back(k)
//    }
}
