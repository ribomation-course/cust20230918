#include <iostream>
#include <numbers>
#include <vector>
#include <memory>

#define abstract 0
using namespace std::numbers;

struct Shape {
    virtual ~Shape() = default;
    virtual double area() const = abstract;
//    Shape(Shape const&) = delete;
//    Shape(Shape&&) noexcept = delete;
};

struct Rectangle : Shape {
    int w, h;
    Rectangle(int w, int h) : w(w), h(h) {}
    double area() const override { return w * h; }
};

struct Triangle : Shape {
    int b, h;
    Triangle(int b, int h) : b(b), h(h) {}
    double area() const override { return b * h / 2.0; }
};

struct Circle : Shape {
    int r;
    Circle(int r) : r(r) {}
    double area() const override { return pi * r * r; }
};

int main() {
    auto const N = 5;
    {
        auto shapes = std::vector<Shape*>{};
        for (auto k = 0; k < N; ++k) {
            Shape* s;
            if (k % 3 == 0) s = new Rectangle{k + 1, k + 3};
            if (k % 3 == 1) s = new Triangle{k + 2, k + 5};
            if (k % 3 == 2) s = new Circle{k + 5};
            shapes.push_back(s);
        }
        for (auto s: shapes) std::cout << s->area() << " ";
        std::cout << "\n";

      //  for (auto s: shapes) delete s;
    }
    std::cout << "----\n";
    {
        auto shapes = std::vector<std::unique_ptr<Shape>>{};
        for (auto k = 0; k < N; ++k) {
            Shape* s;
            if (k % 3 == 0) s = new Rectangle{k + 1, k + 3};
            if (k % 3 == 1) s = new Triangle{k + 2, k + 5};
            if (k % 3 == 2) s = new Circle{k + 5};
            shapes.emplace_back(s);
        }
        for (auto const& s: shapes) std::cout << s->area() << " ";
        std::cout << "\n";
    }
}
