#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <stdexcept>
#include <algorithm>
#include <vector>
#include <unordered_map>
#include <functional>
#include <filesystem>
#include <utility>
#include <random>
#include <chrono>
#include <cctype>

using namespace std;
using namespace std::literals;
namespace fs = std::filesystem;
namespace ch = std::chrono;

using WordFreqTbl = std::unordered_map<std::string, unsigned>;
using WordFreq = std::pair<std::string, unsigned>;

auto load(istream& infile, unsigned minWordSize) -> WordFreqTbl;
auto sorted(WordFreqTbl& frequencies, unsigned maxNumWords) -> vector<WordFreq>;
auto mkTags(vector<WordFreq> words, double minFont, double maxFont) -> vector<string>;
void store(const string& filename, const vector<string>& tags);

random_device dev_random;
default_random_engine r{dev_random()};

int main(int argc, char** argv) {
    auto filename = fs::path{(argc > 1) ? argv[1] : "../../../files/musketeers.txt"s};
    const auto minWordSize = 5U;
    const auto maxNumWords = 100U;
    const auto minFontSize = 15.F;
    const auto maxFontSize = 150.F;

    auto startTime = ch::high_resolution_clock::now();
    auto infile = ifstream{filename};
    if (!infile) throw invalid_argument{"cannot open "s + filename.string()};

    auto frequencies = load(infile, minWordSize);
    auto words = sorted(frequencies, maxNumWords);
    auto tags = mkTags(words, minFontSize, maxFontSize);

    const auto outFilename = filename.filename().replace_extension(".html");
    store(outFilename, tags);
    cout << "written " << outFilename << endl;

    auto endTime = ch::high_resolution_clock::now();
    cout << "elapsed time: " << ch::duration_cast<ch::milliseconds>(endTime - startTime).count() << "ms" << endl;
    cout << "# words: " << frequencies.size() << endl;
    cout << "# bytes: " << fs::file_size(filename) << endl;
}

auto strip(string line) {
    transform(begin(line), end(line), begin(line), [](auto ch) {
        return isalpha(ch) ? tolower(ch) : ' ';
    });
    return line;
}

void accumulate(string const& line,
                const function<bool(string const& word)>& isValid,
                const function<void(string const& word)>& handleWord) {
    auto buf = istringstream{line};
    for (string word; buf >> word;) {
        if (isValid(word)) handleWord(word);
    }
}

auto load(istream& infile, unsigned minWordSize) -> WordFreqTbl {
    auto frequencies = WordFreqTbl{};
    for (auto line = ""s; getline(infile, line);) {
        accumulate(strip(std::move(line)),
                   [minWordSize](string const& word) { return word.size() >= minWordSize; },
                   [&frequencies](string const& word) { ++frequencies[word]; });
    }
    return frequencies;
}

auto sorted(WordFreqTbl& frequencies, unsigned maxNumWords) -> vector<WordFreq> {
    auto sortable = vector<WordFreq>{begin(frequencies), end(frequencies)};
    auto byFreqDesc = [](WordFreq const& lhs, WordFreq const& rhs) { return lhs.second > rhs.second; };
    partial_sort(begin(sortable), begin(sortable) + maxNumWords, end(sortable), byFreqDesc);
    sortable.erase(sortable.begin() + maxNumWords, sortable.end());
    sortable.shrink_to_fit();
    return sortable;
}

string randColor() {
    auto color = uniform_int_distribution<unsigned>{0, 255};
    auto buf = ostringstream{};
    buf << hex << color(r) << color(r) << color(r);
    return buf.str();
}

auto mkTags(vector<WordFreq> words, double minFont, double maxFont) -> vector<string> {
    auto scale = (maxFont - minFont) / (words.front().second - words.back().second);

    auto tags = vector<string>{};
    transform(begin(words), end(words), back_inserter(tags), [scale, minFont](WordFreq& wf) {
        auto [word, count] = wf;
        char buf[500];
        sprintf(buf, "<span style='font-size: %dpx; color: #%s;' title='%d'>%s</span>",
                static_cast<unsigned>(count * scale + minFont),
                randColor().c_str(),
                count,
                word.c_str()
        );
        return string{buf};
    });
    shuffle(begin(tags), end(tags), r);
    return tags;
}


void store(const string& filename, const vector<string>& tags) {
    auto outfile = ofstream{filename};
    if (!outfile) throw invalid_argument{"cannot open "s + filename};

    outfile << R"(
        <html>
        <head>
            <title>Word Frequencies</title>
            <style>
                body {font-family: sans-serif;}
                h1   {text-align: center; color: orange;}
            </style>
        </head>
        <body>
          <h1>Word Frequencies</h1>
    )";
    for (auto const& tag: tags) outfile << tag << endl;
    outfile << R"(
        </body>
        </html>
    )";
}
