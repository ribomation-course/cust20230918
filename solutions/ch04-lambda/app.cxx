#include <iostream>
#include <algorithm>
#include <functional>

using std::cout;
using std::for_each;
using std::transform;

void modify(int* arr, int size, std::function<int(int)> f) {
    for (int i = 0; i < size; ++i) {
        arr[i] = f(arr[i]);
    }
}

unsigned count_if(int* first, int* last, std::function<bool(int)> pred) {
    auto cnt = 0U;
    for (; first != last; ++first) {
        if (pred(*first)) ++cnt;
    }
    return cnt;
}

int main() {
    auto print = [](int n) { cout << n << ' '; };
    {
        int numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        auto const N = sizeof(numbers) / sizeof(numbers[0]);

        for_each(numbers, numbers + N, print);
        cout << "\n";
        auto factor = 42;
        transform(numbers, &numbers[N], numbers, [factor](int n) { return factor * n; });
        for_each(numbers, numbers + N, print);
        cout << "\n";
    }

    {
        int nums[] = {10, 20, 30, 40, 50};
        modify(nums, 5, [](int k) -> int { return k * k; });
        for_each(nums, nums + 5, print);
        cout << "\n";
    }

    {
        int numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        auto const N = sizeof(numbers) / sizeof(numbers[0]);
        auto even = count_if(numbers, numbers + N, [](int n) { return n % 2 == 1; });
        cout << "odd numbers: " << even << "\n";
    }

}
